var turn = false;
var cache = [];
var board = new Array(19);
var isEnd = false;
var isStart = false;
var winner = -1;
var stack =  [];
var numOfPoints = 0;
var time = 30;
var timeClass = "start";

// main
$(document).ready(function(){
 boardSetting();
 $("section #board div").click(clickBoard);
 $("section #left").click(goBack);
 $("section #right").click(goForward);
 $("section .setting-submit").click(timer);
 

});


var timer = function(){
  var a = document.getElementById("time").value;
  if(a.length === 0 && a != 5 && a != 15 && a != 30 && a != 45 && a != 60){
    return;
  }
  $("section .time").removeClass(timeClass);
  $("section .logo").removeClass(timeClass);
 

  if(a == 5){
    timeClass = "start5";
    time = 5;
    return;
  }
  if(a == 15){
    timeClass = "start15";
    time = 15;
    return;
  }
  if(a == 30){
    timeClass = "start30";
    time = 30;
    return;
  }
  if(a == 45){
    timeClass = "start45";
    time = 45;
    return;
  }
  if(a == 60){
    timeClass = "start60";
    time = 60;
    return;
  }
}

//methods

function boardSetting(){
 var cont = document.getElementById("board")
 var x = 0;
 var y = 0;

 for(var i = 0; i < 19; i++){
  board[i] = new Array(19);
 }

 for(var i = 1 ; i < 362 ; i++){
  cache[i] = 0; 
  if(i == 1){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-left-up" data-rol="'+ i +'"></div>';
  }
  else if(i == 19){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-right-up" data-rol="'+ i +'"></div>';
  }
  else if(i == 343){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-left-down" data-rol="'+ i +'"></div>';
  }
  else if(i == 361){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-right-down" data-rol="'+ i +'"></div>';
  }
  else if(i >= 2 && i <=18){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-up" data-rol="'+ i +'"></div>';
  }
  else if(i != 1 && (i-1) % 19 == 0){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-left" data-rol="'+ i +'"></div>';
  }
  else if(i != 19 && i % 19 == 0){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-right" data-rol="'+ i +'"></div>';
  }
  else if(i >= 343 && i <= 361){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-down" data-rol="'+ i +'"></div>';
  }
  else if(i == 61 || i == 67 || i == 73 || i == 175 || i == 181 || i == 187 || i == 289 || i == 295 || i == 301){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-point" data-rol="'+ i +'"></div>';
  }
  else{
   cont.innerHTML += '<div id="pp' + i + '"class="piece" data-rol="'+ i +'"></div>';
  }
  $("#pp" + i).css("top", y*40);
  $("#pp" + i).css("left", x*40);
  
  if(i % 19 == 0){
   y++;
  }
  if(i % 19 == 0){
   x = 0;
  }
  else{
   x++;
  }
 }
}

var clickBoard = function(){
  if(isEnd){
   return;
  }
  if(!isStart){
    TimeCheck(time);
    isStart = true;
  }

  var id =$(this).attr("id");
  var data =$(this).attr("data-rol");
  if(cache[data] == 0){
   var axis = numToAxis(data); 

   if(turn == false){
    $("#" + id).addClass("black");
    $("#turn").removeClass("black");
    $("#turn").addClass("white");
    board[axis.y][axis.x] = 0;         //in board, black = 0;
    turn = true;  
   }
   else{
    $("#" + id).addClass("white");
    $("#turn").removeClass("white");
    $("#turn").addClass("black");
    board[axis.y][axis.x] = 1;         //in board, white = 1;
    turn = false;
   }
   stack.push(axis);
   cache[data] = 1;
   numOfPoints++;
  }

  var bc = boardCheck(board)
  if(bc !== -1){
    isEnd = true;
    winner = win(bc);
    alert(winner + " won");
  }
  resetTimer();
  if(!isEnd) {startTimer();}
}

function numToAxis(num){
 
 var x = (num-1) % 19;
 var y = Math.trunc(num / 19);
 return {x,y};
}

function boardCheck(board){

 //horizontal check 
 for(var i = 0; i < 19; i++){
  var num = 0;
  var color = -1;
  for(var j = 0; j < 19; j++){
   if(board[i][j] === undefined){
    color = -1;
    num = 0;
   }
   else{
    if(color !== board[i][j]){
     color = board[i][j];
     num = 1;
    }
    else{
     num++;
    }
   }
   if(num === 5){ return color;}
  }
 }

 //vertical
 for(var i = 0; i < 19; i++){
  var num = 0;
  var color = -1;
  for(var j = 0; j < 19; j++){
   if(board[j][i] === undefined){
    color = -1;
    num = 0;
   }
   else{
    if(color !== board[j][i]){
     color = board[j][i];
     num = 1;
    }
    else{
     num++;
    }
   }
   if(num === 5){ return color;}
  }
 }

 //대각선 left to right
 for(var i = 0; i < 15; i++){
   for(var j = 0; j < 15; j++){
     var num = 0;
     var color = -1;
     for(var k = 0; k < 5; k++){          
       if(board[i + k][j + k] === undefined){
         break;
       }
       else{
         if(color === -1){
           color = board[i + k][j + k];
           num = 1;
           continue;
         }
         if(color === board[i + k][j + k]){
           num++;
           if(num === 5) { return color;}
           continue;
         }
         break;
       }
     }
   }
 } 

 // 대각선 right to left
 for(var i = 0; i < 15; i++){
   for(var j = 4; j < 19; j++){
   var num = 0;
   var color = -1;
     for(var k = 0; k < 5; k++){
       if(board[i + k][j - k] === undefined){
         break;
       }
       else{
         if(color === -1){
           color = board[i + k][j - k];
           num = 1;
           continue;
         }
         if(color === board[i + k][j - k]){
           num++;
           if(num === 5) { return color;}
           continue;
         }
         break;
       }
     }
   }
 }
 return -1;
}

var goBack = function(){
  if(stack[0] === undefined){
    alert("No more log");
    return;
  }
  var lastMove = stack.pop();
  var color = board[lastMove.y][lastMove.x];
  if(color === 0) {color = "black";}
  else {color = "white";}
  var ppNum = lastMove.y * 19 + lastMove.x + 1;
  $("#pp" + ppNum).removeClass(color);
  cache[ppNum] = 0;
  board[lastMove.y][lastMove.x] = undefined;

  if(turn === false){
    $("#turn").removeClass("black");
    $("#turn").addClass("white");
    turn = true;
  }
  else{
    $("#turn").removeClass("white");
    $("#turn").addClass("black");    
    turn = false;
  }
  numOfPoints--;
  resetTimer();
  startTimer();
}


var goForward = function(){
  alert("Coming soon");
}

function resetTimer(){
  $(".time").removeClass(timeClass);
  $(".logo").removeClass(timeClass);
}

function startTimer(){
  setTimeout(function(){$(".time").addClass(timeClass)}, 100);
  setTimeout(function(){$(".logo").addClass(timeClass)}, 100);
}

function TimeCheck(time){
 
 var curTimeReset = 0;
 var timeSpeed = 1000;
 var curTime = 0;
 
 var timeOut = setInterval(
   
   function(){
     //(document).getElementById("curTime").innerHTML = time - curTime;
     curTime = curTime + 1;
     if(curTimeReset !== numOfPoints){curTime = 0}
     if($(".time").css("width") === "0px"){
       winner = win(!turn);
       isEnd = true;
       alert(winner +" won")
       clearInterval(timeOut)
     }
  
     if(curTime >= time - 15){
       $("#bottom-cover").addClass("on");
       setTimeout(function(){$("#bottom-cover").removeClass("on")}, 100);
     } 
     curTimeReset = numOfPoints
     
   }, timeSpeed);
}

function win(win){
  if(!win){
    return "black";
  }
  else {return "white"};
}

function threeXthree(){
 
}

function fourXfour(){

}

function sixStraight(){
}









