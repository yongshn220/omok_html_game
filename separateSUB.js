
class Pos{
  constructor(x, y){
    this.x = x;
    this.y = y;
  }
  setPos(x,y){
    this.x = x;
    this.y = y;
  }

}

class Candidate{
  constructor(str, lineType, pos){
    this.str = str;
    this.lineType = lineType;
    this.pos = pos;
  }
}

class SearchResult{
  constructor(score, pos){
    this.score = score;
    this.pos = pos;
  }
}

class Map{
  constructor(){
    this.scores = new Array(361);
  }
}

function getCharCount(line, char){
  var count = 0;
  for(var i = 0; i < line.length; i++){
    if(line[i] === char){
      count++;
    }
  }
  return count;
}

function getLineRangeCount(line){
  startPiece = false;
  var count = 0;
  for(var i = 0; i < line.length; i++){
    if(!startPiece && (line[i] === '1')){
      startPiece = true;
    }
    if(startPiece){
      count++;
    }
  }

  for(var i = line.length - 1; i >= 0; i--){
    if(line[i] === '1'){
      break;
    }
    count--;
  }
  return count;
}

function colorToNum(color){
  if(color === "black"){
    return 0;
  }
  return 1;
}

function numToColor(num){
  if(num === 0){
    return "black";
  }
  else if(num === 1){
    return "white";
  }
  else{
    return -1;
  }
}

function oppositeColor(color){
  if(color == "black"){
    return "white";
  }
  else{
    return "black";
  }
}

function arrayCopy(A){
  var newArray = [];
  for(var i = 0; i < A.length; i++){
    newArray[i] = A[i];
  }
  return newArray;
}

//apply following offset into Pos. 
function posOffsetting(startPos, offset){
  startPos.setPos(startPos.x + offset.x, startPos.y + offset.y)
  //startPos.x = startPos.x + offset.x;
  //startPos.y = startPos.y + offset.y;

}

//return the following Pos(offset) of the line type.
function getOffset(lineType){
  switch(lineType){
    case "col" : return new Pos(1, 0);
    case "row" : return new Pos(0, 1);
    case "slash-left" : return new Pos(1, 1);
    case "slash-right" : return new Pos(-1, 1);
  }
}

//copy class Board and return a new class Board.
function boardCopy(A){
  var newBoard = new Array(19);
  for(var i = 0; i < 19; i++){
    newBoard[i] = new Array(19);
  }

  for(var i = 0; i < 19; i++){
    for(var j = 0; j < 19; j++){
      newBoard[i][j] = A[i][j];
    }
  }
  return newBoard;
}

//check the following string is all zeros.
function allZerosLineStrCheck(lineStr){
  for(var i = 0; i < lineStr.length; i++){
    var part = lineStr[i];
    if(part !== '0'){
      return false;
    }
  }
  return true; 
}

//check the following string is empty.
function emptyLineStrCheck(lineStr){
  if(lineStr === ""){
    return true;
  }
  return false;
}

//copy class Pos and return new class Pos;
function posCopy(pos){
  return new Pos(pos.x, pos.y);
}

//replace the specific position of string into following character.
function strCharReplace(string, index, char){
  var stringA = string.substring(0, index);
  var stringB = string.substring(index + 1);
  return stringA + char + stringB;
}

//replace string's first and last into 'S'.
function replaceSideIntoS(center, first, last){
  var newFirst = first;
  var newLast = last;

  if(first === "00"){
    newFirst = "SS";
  }
  else if(first === "0"){
    newFirst = "S";
  }
  if(last === "00" || last == "0"){
    newLast = "SS";
  }
  else if(last === "0"){
    newLast = "S";
  }
  return newFirst + center + newLast;
}

//replace string's first and last into 'S' except the closest 'O' from the center.
function replaceSideIntoSLeftOne(center, first, last){
  var newFirst = first;
  var newLast = last;

  if((first === "" || last === "") && center.length === 3){
    return first + center + last;
  }

  if(first === "00"){
    newFirst = "S0";
  }
  else if(first === "0" && center !== "1111"){
    newFirst = "S";
  }

  if(last === "00"){
    newLast = "0S";
  }
  else if(last === "0" && center !== "1111"){
    newLast = "S";
  }
  return newFirst + center + newLast;
}

function numToAxis(num){
 var x = (num-1) % 19;
 var y = Math.trunc((num-1) / 19);
 return {x,y};
}

function posToNum(pos){
  return pos.y * 19 + pos.x;
}

