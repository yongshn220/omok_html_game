
// show board image on screen.
function boardSetting(){
 var cont = document.getElementById("board")
 var x = 0;
 var y = 0;
 var board = [];

 for(var i = 0; i < 19; i++){
   var boardx = [];
   for(var j = 0; j < 19; j++){
     boardx.push(-1);
   }
   board.push(boardx);
 }

 for(var i = 1 ; i < 362 ; i++){
  cache[i] = 0; 
  if(i == 1){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-left-up" data-rol="'+ i +'"></div>';
  }
  else if(i == 19){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-right-up" data-rol="'+ i +'"></div>';
  }
  else if(i == 343){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-left-down" data-rol="'+ i +'"></div>';
  }
  else if(i == 361){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-right-down" data-rol="'+ i +'"></div>';
  }
  else if(i >= 2 && i <=18){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-up" data-rol="'+ i +'"></div>';
  }
  else if(i != 1 && (i-1) % 19 == 0){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-left" data-rol="'+ i +'"></div>';
  }
  else if(i != 19 && i % 19 == 0){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-right" data-rol="'+ i +'"></div>';
  }
  else if(i >= 343 && i <= 361){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-down" data-rol="'+ i +'"></div>';
  }
  else if(i == 61 || i == 67 || i == 73 || i == 175 || i == 181 || i == 187 || i == 289 || i == 295 || i == 301){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-point" data-rol="'+ i +'"></div>';
  }
  else{
   cont.innerHTML += '<div id="pp' + i + '"class="piece" data-rol="'+ i +'"></div>';
  }
  $("#pp" + i).css("top", y*40);
  $("#pp" + i).css("left", x*40);
  
  if(i % 19 == 0){
   y++;
  }
  if(i % 19 == 0){
   x = 0;
  }
  else{
   x++;
  }
 }
 return board;
}
