
// main
$(document).ready(function(){
 // var testS = "00001110000";
 // console.log(lineFiltering(testS));
});

function partTrimTwo(lineStr, destinates){
  if(lineStr.length < 2) return false;
  if(getLineRangeCount(lineStr) <= 5) return false;
  var zeroCount = getCharCount(lineStr, '0');
  if(zeroCount <= 0) return false;
  
  var last1 = lineStr.lastIndexOf('1');
  var idx = lineStr.lastIndexOf('0', last1);
  
  destinates.dst1 = lineStr.substring(0, idx);
  destinates.dst2 = lineStr.substring(idx);
  return true;
}

function partTrimSide(lineStr, side){

  var n1 = lineStr.search('1');
  var n2 = lineStr.lastIndexOf('1');

  side.first = lineStr.substring(0, n1);
  side.last = lineStr.substring(n2 + 1);
  side.center = lineStr.substring(n1, n2 + 1);
}

function partTrim(lineStr, destAndRemainder){
  var side = {first : "", last : "", center : ""};

  partTrimSide(lineStr, side);

  var first = side.first;
  var center = side.center;
  var last = side.last;

  if(first == undefined || center == undefined || last == undefined){
    console.log("a");
  }

  var zeroCount = getCharCount(center, '0');
  var oneCount = getCharCount(center, '1');
  if(zeroCount + oneCount <= 5) {
    destAndRemainder.dest = lineStr;
    return false
  };
  
  var destinates = {dst1 : "", dst2 : "",};      // destinates = [dst1, dst2] 
  destAndRemainder.dest = center;
  if(partTrimTwo(destAndRemainder.dest, destinates)){
    destAndRemainder.dest = first + destinates.dst1;
    destAndRemainder.remain = destinates.dst2 + last;
  }
  else{
    destAndRemainder.dest = first + destAndRemainder.dest;
    return false;
  }
  return true;
}



/* 
receive each line of board as a string, separate it into parts, and store it in the 'candidates'.
*/
function partSeparate(lineStr, lineType, startPos, candidates){
  if(emptyLineStrCheck(lineStr)) {return true;}
  if(allZerosLineStrCheck(lineStr)) {return true;}
  if(lineStr == undefined){ 
    console.log("partSeparate : lineString undefined error");
    return true;
  }

  lineStr = lineAdjusting(lineStr, lineType, startPos);

  var tempLineStr = lineStr;
  var tempPos = posCopy(startPos);
  var loop = true;
  
  // loop until the string is separated all.
  while(loop){
    var destAndRemainder = {dest : "", remain : ""};   // destAndRemainder = [destinate, remainder]
    loop = partTrim(tempLineStr, destAndRemainder);  // false if there is no more remainders.
    var newTempPos = posCopy(tempPos);
    var newDest = lineFiltering(destAndRemainder.dest);
    if(newDest === false){
      return false;
    }
    var newCandidate = new Candidate(newDest, lineType, newTempPos); // store new candidate.
    candidates.push(newCandidate);
    if(!loop) break;
    // apply offset for separated string.
    for(var i = 0; i < destAndRemainder.dest.length; i++){  
      posOffsetting(tempPos, getOffset(lineType));
    }
    if(destAndRemainder.remain == undefined){
      console.log("a");
    }    
    tempLineStr = destAndRemainder.remain;        // next separated string = remainders
  }
  return true;
}

function lineFiltering(lineStr){
  var side = {first : "", last : "", center : ""};
  partTrimSide(lineStr, side);
  if(side.first == undefined || side.center == undefined || side.last == undefined){
    console.log("A");
  }
  var zeroCount = getCharCount(side.center, '0');
  var oneCount = getCharCount(side.center, '1');
  var centerLen = side.center.length;
  
  if(centerLen === 5){
    //001111100
    if(zeroCount === 0){
      return false;
    }
    //001101100 or 001011100, 001110100, 001001100, 001100100, 01000100 -> SS*****SS
    if(zeroCount === 1 || zeroCount === 2 || zeroCount === 3){
      return replaceSideIntoS(side.center, side.first, side.last);
    }
  }
  
  else if(centerLen === 4){
    //00111100 -> S011110S
    if(zeroCount === 0){
      return replaceSideIntoSLeftOne(side.center, side.first, side.last);
    }
    //00110100 or 00101100  -> SS1101SS or SS1011SS
    else if(zeroCount === 1 && side.first.length > 0 && side.last.length > 0){
      return replaceSideIntoS(side.center, side.first, side.last);
    }
  }
  
  else if(centerLen === 3){
    //0011100 -> S01110S
    if(zeroCount === 0){
      return replaceSideIntoSLeftOne(side.center, side.first, side.last);
    }
    //0010100 -> S01010S
    else if(zeroCount === 1){
      if(side.center[0] === '1' && side.center[1] === '0' && side.center[2] === '1'){
        return replaceSideIntoSLeftOne(side.center, side.first, side.last);
      }
    }
  }
  /*
  else if(centerLen === 2){
    var tempLineFirst = "";
    var tempLineLast = "";
    if(side.first === '0'){
      tempLineFirst = "S";
    }
    else if(side.last === '0' && tempLineFirst !== "S"){
      //tempLineLast = "S";
    }
    else{
      return lineStr;
    }
    return tempLineFirst + side.cneter + tempLineLast;
  }
  */

  else{
    return lineStr;
  }
  return lineStr;
}



/* 
receive each line from board and make it into string.
*/
function lineSeparate(line, color, lineType, startPos, candidates){
  //기저사례
  if(line.length === 0){
    return true;
  }
  var oppoColor = oppositeColor(color);
  var curNum = colorToNum(color);
  var oppoNum = colorToNum(oppoColor);
  var offset = getOffset(lineType);
  var newLine = arrayCopy(line);
  var lineStr = '';
  var newStartPos = posCopy(startPos);
  
  for(var i = 0; i < newLine.length; i++){
    var linePiece = newLine[i];
    if(linePiece === -1){
      lineStr += '0';
    }
    else if(linePiece === curNum){
      lineStr += '1';
    }
    else if(linePiece === oppoNum) {
      for(var j = 0; j < i + 1; j++){
        posOffsetting(newStartPos, offset);
        newLine.shift();
        
      }
      if(!lineSeparate(newLine, color, lineType, newStartPos, candidates)){
        return false;
      }
      break;
    }
    else{
      console.log("exception: " + linePiece);
      break;
    }
  }

  if(!partSeparate(lineStr, lineType, startPos, candidates)){
    return false;
  }
  return true;
}


function lineAdjusting(lineStr, lineType, startPos){
  var isFrontEnd = false;
  var isRearEnd = false;
  while(!isFrontEnd || !isRearEnd){
    if(!isFrontEnd && lineStr[0] === '0' && lineStr[1] === '0' && lineStr[2] === '0'){
      lineStr = lineStr.substring(1);
      posOffsetting(startPos, getOffset(lineType));
    }
    else{
      isFrontEnd = true;
    }

    if(!isRearEnd && lineStr[lineStr.length - 1] === '0' && lineStr[lineStr.length - 2] === '0' && lineStr[lineStr.length - 3] === '0'){
      lineStr = lineStr.substring(0, lineStr.length - 1);
    }
    else{
      isRearEnd = true;
    }   
  }
  return lineStr;
}



//현재 플레이어의 보드 상황을 전송
function boardSeparate(board, color, candidates){
  var newBoard = boardCopy(board);
  if(!colSeparate(newBoard, color, candidates)){ return false; }
  if(!rowSeparate(newBoard, color, candidates)){ return false; }
  if(!slashLeftSeparate(newBoard, color, candidates)){ return false; }
  if(!slashRightSeparate(newBoard, color, candidates)){ return false;}
  return true;
}

// 보드 가로 읽기
function colSeparate(board, color, candidates){
  var lineType = "col";
  var startPos;
  for(var i = 0; i < 19; i++){
    startPos = new Pos(0, i);
    if(!lineSeparate(board[i], color, lineType, startPos, candidates)){ return false; }
  }
  return true;
}
//보드 세로 읽기
function rowSeparate(board, color, candidates){
  var lineType = "row";
  var startPos;
  for(var i = 0; i < 19; i++){
    line = [];
    startPos = new Pos(i, 0);
    for(var j = 0; j < 19; j++){
      line.push(board[j][i]);
    }
    if(!lineSeparate(line, color, lineType, startPos, candidates)){ return false; }
  }
  return true;
}

//보드 slash-left 읽기
function slashLeftSeparate(board, color, candidates){
  var lineType = "slash-left";
  var startPos;
  for(var i = 0; i < 19; i++){
    line = [];
    startPos = new Pos(0, i);
    for(var j = 0; j < 19 - i; j++){
      line.push(board[j + i][j]);
    }
    if(!lineSeparate(line, color, lineType, startPos, candidates)){ return false; }
    
    if(i === 18) {break;}
    line = [];
    startPos = new Pos(i + 1, 0);
    for(var j = 0; j < 18 - i; j++){
      line.push(board[j][j + i + 1]);
    }
    if(!lineSeparate(line, color, lineType, startPos, candidates)){ return false; }
  }
  return true;
}

//보드 slash-right 읽기
function slashRightSeparate(board, color, candidates){
  var lineType = "slash-right";
  var startPos;
  for(var i = 18; i >= 0; i--){
    line = [];
    startPos = new Pos(i, 0);
    for(var j = 0; j < i + 1; j++){
      line.push(board[j][i - j]);
    }
    if(!lineSeparate(line, color, lineType, startPos, candidates)){ return false; }
  }
  for(var i = 1; i < 19; i++){
    line = [];
    startPos = new Pos(18, i);
    for(var j = 18; j >= i; j--){
      line.push(board[i + (18 - j)][j]);
    }
    if(!lineSeparate(line, color, lineType, startPos, candidates)){ return false; }
  }
  return true;
}
