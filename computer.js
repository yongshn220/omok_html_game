
var cache = [];
var board;
var isEnd = false;
var winner = -1;
var stack =  [];

//******* main *******************************************************
$(document).ready(function(){
 board = boardSetting();
 $("section #board div").click(clickBoard); 
 AITurn();

});

//********************************************************************

// click event : set white piece on board and moves to the black's turn.
var clickBoard = function(){
  if(isEnd){ return; }
  var id =$(this).attr("id");
  var data =$(this).attr("data-rol");   
  if(!setWhiteOnScreen(id, data)){ return false; } // if the position is already on, return. 
  if(isWinner(board)){ return; } 
  AITurn(); // next is AI turn.
}


// AI turn start. set black piece on the best location.
function AITurn(){
  var best = searchBestLocation(board);
  var bestLoc = best.pos;
  console.log("current score of computer : " + best.score);
  setBlackOnScreen(bestLoc);
  isWinner(board);
  console.log("best location: " + "(" + bestLoc.x  + ", " +  bestLoc.y + ")");
}

// set white piece on board.
function setWhiteOnScreen(id, data){
    if(cache[data] !== 0){ return false; }
    cache[data] = 1;
    var axis = numToAxis(data);    
    $("#" + id).addClass("white");
    $("#turn").removeClass("white");
    $("#turn").addClass("black");
    board[axis.y][axis.x] = 1;         //in board, white = 1;
    return true;
}

//set black piece on board.
function setBlackOnScreen(pos){
  var id = posToNum(pos) + 1;
  board[pos.y][pos.x] = 0;         //in board, black = 0;
  cache[id] = 1;
  $("#pp" + id).addClass("black");
  $("#turn").removeClass("black");
  $("#turn").addClass("white");
}

//check winner
function isWinner(board){
  var check = boardCheck(board);
  var winner = numToColor(check);
  if(winner === -1){
    return false;
  }
  else{
    alert(winner + " won");
    isEnd = true;
    return true;
  }
}

//check winner from the board in 4 ways 
function boardCheck(board){
 //col
 for(var i = 0; i < 19; i++){
  var num = 0;
  var color = -1;
  for(var j = 0; j < 19; j++){
   if(board[i][j] === -1){
    color = -1;
    num = 0;
   }
   else{
    if(color !== board[i][j]){
     color = board[i][j];
     num = 1;
    }
    else{
     num++;
    }
   }
   if(num === 5){ return color;}
  }
 }

 //row
 for(var i = 0; i < 19; i++){
  var num = 0;
  var color = -1;
  for(var j = 0; j < 19; j++){
   if(board[j][i] === -1){
    color = -1;
    num = 0;
   }
   else{
    if(color !== board[j][i]){
     color = board[j][i];
     num = 1;
    }
    else{
     num++;
    }
   }
   if(num === 5){ return color;}
  }
 }

 //slash-left
 for(var i = 0; i < 15; i++){
   for(var j = 0; j < 15; j++){
     var num = 0;
     var color = -1;
     for(var k = 0; k < 5; k++){          
       if(board[i + k][j + k] === -1){
         break;
       }
       else{
         if(color === -1){
           color = board[i + k][j + k];
           num = 1;
           continue;
         }
         if(color === board[i + k][j + k]){
           num++;
           if(num === 5) { return color;}
           continue;
         }
         break;
       }
     }
   }
 } 

 // slash-right
 for(var i = 0; i < 15; i++){
   for(var j = 4; j < 19; j++){
   var num = 0;
   var color = -1;
     for(var k = 0; k < 5; k++){
       if(board[i + k][j - k] === -1){
         break;
       }
       else{
         if(color === -1){
           color = board[i + k][j - k];
           num = 1;
           continue;
         }
         if(color === board[i + k][j - k]){
           num++;
           if(num === 5) { return color;}
           continue;
         }
         break;
       }
     }
   }
 }
 return -1;
}


function win(win){
  if(!win){
    return "black";
  }
  else {return "white"};
}





