var turn = false;
var cache = [];
var board = new Array(19);
var isEnd = false;
var isStart = false;
var winner = -1;
var numOfPoints = 0;

var blackBarTime;
var whiteBarTime;


// main
$(document).ready(function(){
 boardSetting();
 $("section #board div").click(clickBoard);
 $("section #right").click(goForward);
});

//methods

function boardSetting(){
 var cont = document.getElementById("board")
 var x = 0;
 var y = 0;

 for(var i = 0; i < 19; i++){
  board[i] = new Array(19);
 }

 for(var i = 1 ; i < 362 ; i++){
  cache[i] = 0; 
  if(i == 1){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-left-up" data-rol="'+ i +'"></div>';
  }
  else if(i == 19){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-right-up" data-rol="'+ i +'"></div>';
  }
  else if(i == 343){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-left-down" data-rol="'+ i +'"></div>';
  }
  else if(i == 361){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-right-down" data-rol="'+ i +'"></div>';
  }
  else if(i >= 2 && i <=18){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-up" data-rol="'+ i +'"></div>';
  }
  else if(i != 1 && (i-1) % 19 == 0){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-left" data-rol="'+ i +'"></div>';
  }
  else if(i != 19 && i % 19 == 0){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-right" data-rol="'+ i +'"></div>';
  }
  else if(i >= 343 && i <= 361){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-down" data-rol="'+ i +'"></div>';
  }
  else if(i == 61 || i == 67 || i == 73 || i == 175 || i == 181 || i == 187 || i == 289 || i == 295 || i == 301){
   cont.innerHTML += '<div id="pp' + i + '"class="piece-point" data-rol="'+ i +'"></div>';
  }
  else{
   cont.innerHTML += '<div id="pp' + i + '"class="piece" data-rol="'+ i +'"></div>';
  }
  $("#pp" + i).css("top", y*40);
  $("#pp" + i).css("left", x*40);
  
  if(i % 19 == 0){
   y++;
  }
  if(i % 19 == 0){
   x = 0;
  }
  else{
   x++;
  }
 }
}

var clickBoard = function(){
  if(isEnd){
   return;
  }
  if(!isStart){
    isStart = true;
  }

  var id =$(this).attr("id");
  var data =$(this).attr("data-rol");
  if(cache[data] == 0){
   var axis = numToAxis(data); 

   if(turn == false){
    $("#" + id).addClass("black");
    $("#turn").removeClass("black");
    $("#turn").addClass("white");
    board[axis.y][axis.x] = 0;         //in board, black = 0;
    turn = true;
    whiteOn();
   }
   else{
    $("#" + id).addClass("white");
    $("#turn").removeClass("white");
    $("#turn").addClass("black");
    board[axis.y][axis.x] = 1;         //in board, white = 1;
    turn = false;
    blackOn();
   }
   cache[data] = 1;
   numOfPoints++;
  }

  var bc = boardCheck(board)
  if(bc !== -1){
    isEnd = true;
    winner = win(bc);
    alert(winner + " won");
  }
}

function numToAxis(num){
 
 var x = (num-1) % 19;
 var y = Math.trunc(num / 19);
 return {x,y};
}

function boardCheck(board){

 //horizontal check 
 for(var i = 0; i < 19; i++){
  var num = 0;
  var color = -1;
  for(var j = 0; j < 19; j++){
   if(board[i][j] === undefined){
    color = -1;
    num = 0;
   }
   else{
    if(color !== board[i][j]){
     color = board[i][j];
     num = 1;
    }
    else{
     num++;
    }
   }
   if(num === 5){ return color;}
  }
 }

 //vertical
 for(var i = 0; i < 19; i++){
  var num = 0;
  var color = -1;
  for(var j = 0; j < 19; j++){
   if(board[j][i] === undefined){
    color = -1;
    num = 0;
   }
   else{
    if(color !== board[j][i]){
     color = board[j][i];
     num = 1;
    }
    else{
     num++;
    }
   }
   if(num === 5){ return color;}
  }
 }

 //대각선 left to right
 for(var i = 0; i < 15; i++){
   for(var j = 0; j < 15; j++){
     var num = 0;
     var color = -1;
     for(var k = 0; k < 5; k++){          
       if(board[i + k][j + k] === undefined){
         break;
       }
       else{
         if(color === -1){
           color = board[i + k][j + k];
           num = 1;
           continue;
         }
         if(color === board[i + k][j + k]){
           num++;
           if(num === 5) { return color;}
           continue;
         }
         break;
       }
     }
   }
 } 

 // 대각선 right to left
 for(var i = 0; i < 15; i++){
   for(var j = 4; j < 19; j++){
   var num = 0;
   var color = -1;
     for(var k = 0; k < 5; k++){
       if(board[i + k][j - k] === undefined){
         break;
       }
       else{
         if(color === -1){
           color = board[i + k][j - k];
           num = 1;
           continue;
         }
         if(color === board[i + k][j - k]){
           num++;
           if(num === 5) { return color;}
           continue;
         }
         break;
       }
     }
   }
 }
 return -1;
}


var goForward = function(){
  alert("");
}

function blackOn(){
  clearInterval(whiteBarTime);
  blackBarTime = setInterval(
    function(){
      var width = $("#player-black .time").css("width");
      width = width.replace("px","");
      if(width == 0){
        alert("white won");
        clearInterval(blackBarTime);
      } 
      $("#player-black .time").css("width", width - 0.6);
    }
  , 500);
}
function whiteOn(){
  clearInterval(blackBarTime);
    whiteBarTime = setInterval(
    function(){
      var width = $("#player-white .time").css("width");
      width = width.replace("px","");
      if(width == 0){
        alert("black won");
        clearInterval(whiteBarTime);
      } 
      $("#player-white .time").css("width", width-0.6);
    }
  , 500);
}

function win(win){
  if(!win){
    return "black";
  }
  else {return "white"};
}

function threeXthree(){
 
}

function fourXfour(){

}

function sixStraight(){
}









