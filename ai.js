
//search best location.
function searchBestLocation(board){
  var searchR = aiSearchTree(board, 7, -987654, 987654, "black");
  return searchR;  
}


function aiSearchTree(board, depth, alpha, beta, color){

    var candidates = [];
    var oppoCandidates = [];

    var isColorWin = boardSeparate(board, color, candidates);
    var isOppoColorWin = boardSeparate(board, oppositeColor(color), oppoCandidates);

    if(candidates.length === 0 && oppoCandidates.length === 0){
        return new SearchResult(0, new Pos(9,9));
    }

    if(!isColorWin || !isOppoColorWin){
        if(!isColorWin){
            if(color === "white"){
                return new SearchResult(-1000, new Pos(-1, -1));
            }
            else{
                return new SearchResult(1000, new Pos(-1, -1));
            }
        }
        else{
            if(color === "white"){
                return new SearchResult(1000, new Pos(-1, -1));
            }
            else{
                return new SearchResult(-1000, new Pos(-1, -1));
            }         
        }
    }
    
    if(depth === 0){
        var scoreDifference = compareTwoCandidates(candidates, oppoCandidates);
        if(color === 'white'){
          scoreDifference = -(scoreDifference);
        }

        if(scoreDifference >= -1000 || scoreDifference <= 1000){
        }
        else{
          console.log("NaN");
        }
        //printBoard(board);
        //console.log("score : " + scoreDifference);
        return new SearchResult(scoreDifference, new Pos(-1, -1));
    }
    
    



    var scoreMap = new Map();
    var oppoScoreMap = new Map();

    setScoreToMap(scoreMap, candidates, true);
    setScoreToMap(oppoScoreMap, oppoCandidates, false);
    
    /*
    if(depth === 0){
        var scoreDifference = compareTwoCandidatesTest(scoreMap, oppoScoreMap);
        if(color === 'white'){
          scoreDifference = -(scoreDifference);
        }

        if(scoreDifference >= -1000 || scoreDifference <= 1000){
        }
        else{
          console.log("NaN");
        }
        //printBoard(board);
        //console.log("score : " + scoreDifference);
        return new SearchResult(scoreDifference, new Pos(-1, -1));
    }
    */


    adjustCandidatePosition(scoreMap, candidates);
    adjustCandidatePosition(oppoScoreMap, oppoCandidates);

    var mixedCandidates = mixCandidates(candidates, oppoCandidates);
    var mixedMap = addMaps(scoreMap, oppoScoreMap);
    candidateSortTest(mixedCandidates, mixedMap);

    if(color === "black"){
        var maxSearchResult = new SearchResult(-987654, new Pos(0, 0));
        var curCnt = 0;
        var maxCnt = 4;
        for(var i = 0; i < candidates.length; i++){
            if(++curCnt >= maxCnt){
                break;
            }
            var curCand = mixedCandidates[i];
            const curCandPos = curCand.pos;

            var newBoard = boardCopy(board);
            setPieceOnBoard(newBoard, color, curCandPos);
            var searchR = aiSearchTree(newBoard, depth - 1, alpha, beta, oppositeColor(color))
            if(maxSearchResult.score < searchR.score){
                maxSearchResult.score = searchR.score;
                maxSearchResult.pos = curCandPos;
            }
            alpha = Math.max(alpha, searchR.score);
            if(beta <= alpha){
                break;
            }
        }

        return maxSearchResult;
    }
    else{
        var minSearchResult = new SearchResult(987654, new Pos(0, 0));
        var curCnt = 0;
        var maxCnt = 4;
        for(var i = 0; i < candidates.length; i++){
            if(++curCnt >= maxCnt){
                break;
            }
            var curCand = mixedCandidates[i];
            const curCandPos = curCand.pos;
            var newBoard = boardCopy(board);
            setPieceOnBoard(newBoard, color, curCandPos);

            var searchR = aiSearchTree(newBoard, depth - 1, alpha, beta, oppositeColor(color))
            if(minSearchResult.score > searchR.score){
                minSearchResult.score = searchR.score;
                minSearchResult.pos = curCandPos;
            }
            beta = Math.min(beta, searchR.score);
            if(beta <= alpha){
                break;
            }
        }
        return minSearchResult;
    }
}

function printBoard(board){
  var tempBoard = new Array(19);
  for(var i = 0; i< 19; i++){
    tempBoard[i] = new Array(19);
  }
  for(var i = 0; i< 19; i++){
     for(var j = 0; j< 19; j++){
        if(board[i][j] === -1){
          tempBoard[i][j] = 3;
        }
        else if(board[i][j] === 0){
          tempBoard[i][j] = 0;
        }
        else{
          tempBoard[i][j] = 1;
        }
     }
    
  }
  console.log(tempBoard);
}

function mixCandidates(candidates, oppoCandidates){
  var mixedCands = [];
  for(var i = 0; i < candidates.length; i++){
    mixedCands.push(candidates[i]);
  }
  for(var i = 0; i < oppoCandidates.length; i++){
    mixedCands.push(oppoCandidates[i]);
  }
  return mixedCands;
}

function setPieceOnBoard(board, piece, pos){
    board[pos.y][pos.x] = colorToNum(piece);
}


function candidateSort(candidates){
   // var sCandidates = [];
    candidates.sort(function(a, b){
        return getLineStringScore(b.str) - getLineStringScore(a.str);
    });
}


function candidateSortTest(candidates, map){
    candidates.sort(function(a, b){
        var test1 = map.scores[posToNum(b.pos)] 
        var test2 = map.scores[posToNum(a.pos)]
        return test1 - test2;
    });
   
}

function addMaps(map1, map2){
  var newMap = new Map();
  for(var i = 0; i < map1.scores.length; i++){
    newMap.scores[i] = map1.scores[i];
  }
  for(var i = 0; i < map2.scores.length; i++){
    newMap.scores[i] += map2.scores[i];
  }
  return newMap;
}



function adjustCandidatePosition(map, candidates){

  for(var i = 0; i < candidates.length; i++){
      var curCandidate = candidates[i];
      var curLine = curCandidate.str;
      var curPos = curCandidate.pos;
      var offset = getOffset(curCandidate.lineType);

      var highestScore = -1;
      var highestPos = new Pos(-1, -1);
      var tempPos = posCopy(curPos);
      for(var j = 0; j < curLine.length; j++){
          var tempScore = map.scores[posToNum(tempPos)]
          if(curLine[j] === '0' && highestScore < tempScore){
              highestScore = tempScore;
              highestPos = posCopy(tempPos);
              
          }
          posOffsetting(tempPos, offset);

      }
      if(highestScore === -1){
        for(var k = 0; k < curLine.search('S'); k++){
          posOffsetting(curPos, offset);
        }
        highestPos = posCopy(curPos);
      }
      curCandidate.pos = highestPos;
  }
}


function getLineStringScore(lineStr){
  var side = {first : "", last : "", center : ""};
  partTrimSide(lineStr, side);

  var pieceCnt = getCharCount(side.center, '1');
  var emptyCnt = getCharCount(side.center, '0');
  var firstCnt = side.first.length;
  var lastCnt = side.last.length;
  var length = lineStr.length;

  if(pieceCnt >= 6){
    return 1000;
  }

  else if(pieceCnt === 5){
     return 1000; 
  }

  if(pieceCnt === 4){
      if(length > 5){
          return 500;
      }
      else if(length === 5){
          return 400;
      }
      else{
          return 0;
      }
  }
  else if(pieceCnt === 3){
      if(length <= 4){
          return 0;
      }
      else{
          if((emptyCnt <= 1) && (firstCnt >= 1) && (lastCnt >= 1)){
              return 50;
          }
          else if((emptyCnt <= 1) && ((firstCnt >= 1) || (lastCnt >=  1))){
              return 30;
          }
          else if(emptyCnt <= 2){
              return 30;
          }
          else{
              return 0;
          }
      }
  }
  else if(pieceCnt === 2){
      if(length < 4){
          return 0;
      }
      else if((emptyCnt === 0) && (length < 5)){
          return 0;
      }
      else if((firstCnt >= 1) || (lastCnt >= 1)){
          return 20;
      }
      else{
          return 0;
      }
  }
  else if(pieceCnt === 1){
      if((firstCnt >= 2) || (lastCnt >= 2)){
          return 5;
      }
      else{
          return 0;
      }
  }
}

function compareTwoCandidates(candidatesA, candidatesB){
    var scoreA = 0;
    var scoreB = 0;
    var hightestA = 0;
    var hightestB = 0;
    
    for(var i = 0; i < candidatesA.length; i++){
        scoreA = scoreA + getLineStringScore(candidatesA[i].str);
    }
    for(var i = 0; i < candidatesB.length; i++){
        scoreB = scoreB + getLineStringScore(candidatesB[i].str);
    }
    return scoreA - scoreB;
}


function compareTwoCandidatesTest(mapA, mapB){
    var scoreA = 0;
    var scoreB = 0;
    var hightestA = 0;
    var hightestB = 0;
    
    for(var i = 0; i < mapA.length; i++){
        scoreA += mapA[i];
        scoreB += mapB[i];
    }

    return scoreA - scoreB;
}


function setScoreToMap(map, candidates, isCurColor){
  for(var i = 0; i < 361; i++){
    if(map.scores[i] === undefined){
      map.scores[i] = 0;
    }
  }

  for(var i = 0; i < candidates.length; i++){
    var curCand = candidates[i];
    var curPos = posCopy(curCand.pos);
    var tempNum = 0;
    var curLineStrScore = getLineStringScore(curCand.str);
    for(var j = 0; j < curCand.str.length; j++){
      if(curCand.str[j] === '0'){
        tempNum = posToNum(curPos);
        map.scores[tempNum] += curLineStrScore;
        if(curLineStrScore >= 50 && !isCurColor){
          map.scores[tempNum] += 250;
        }
      }
      posOffsetting(curPos, getOffset(curCand.lineType));
    }
  }
  if(isCurColor){
    for(var i = 0; i < 361; i++){
      if(map.scores[i] !== 0){
        map.scores[i] += 15;
      }
    }
  }
}